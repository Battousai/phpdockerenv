Têm de ter instalado o dnsmasq (já têm se tiverem valet, senão instalar com brew). Adicionar a linha `address=/.docker/127.0.0.1` ao ficheiro `~/.valet/dnsmasq.conf`.
 
Criar ficheiro `/etc/resolver/docker` com o conteúdo `nameserver 127.0.0.1`. Reiniciar o dnsmasq: `sudo brew services restart dnsmasq`.
 
Instalar docker para mac.
 
Criar uma rede: `docker network create nginx_proxified`.
 
Disparar o reverse proxy: `docker run -d -p 8000:80 -v /var/run/docker.sock:/tmp/docker.sock:ro --name=nginx-proxy --net=nginx_proxified jwilder/nginx-proxy`.

Clonar o repositório, mudar para a branch mac-os e executar `docker-compose up`. Aceder a `alpine.docker:8000`. Porto 8000 para não entrar em conflito com valet.
